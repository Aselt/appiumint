package com.techtorial.appium.ecommerce;

import com.techtorial.appium.utils.AndroidDriverUtil;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;

public class EcommerceIntro {
  AndroidDriver<AndroidElement> driver;

  @Before
  public void setup() throws MalformedURLException {
    DesiredCapabilities desiredCapabilities = AndroidDriverUtil.setupDesiredCapabilities("ecommerceApp");
    driver = AndroidDriverUtil.getAndroidDriver(desiredCapabilities);
  }

  @Test
  public void test1(){
     LoginPage loginPage = new LoginPage(driver);
    loginPage.country.click();

    //AndroidElement bolivia = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Bolivia\"));");
    loginPage.bolivia.click();
    loginPage.username.click();
    loginPage.username.sendKeys("techtorial");
    if(driver.isKeyboardShown()){
      driver.hideKeyboard();
    }

   loginPage.loginButton.click();

  }


  @Test
  public void loginTestNegative(){
    LoginPage loginPage = new LoginPage(driver);
    loginPage.country.click();

    loginPage.loginButton.click();

    AndroidElement toastMessage = driver.findElement(By.xpath("//android.widget.Toast"));
    String message = toastMessage.getAttribute("name");

    Assert.assertEquals("Please enter your name", message);


    loginPage.loginButton.click();

  }

  /*
  loging to ecommerce app
  Add ther Air Jordan 9 retro shoues to cart
  verify those shoes are in the cart
   */
  @Test
  public void addCardShoes(){
    LoginPage loginPage = new LoginPage(driver);
    loginPage.country.click();

    //AndroidElement bolivia = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Bolivia\"));");
    loginPage.bolivia.click();
    loginPage.username.click();
    loginPage.username.sendKeys("techtorial");
    if(driver.isKeyboardShown()){
      driver.hideKeyboard();
    }

    loginPage.loginButton.click();
    AndroidElement jardonShoes9 = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Air Jordan 9 Retro\"));");
   AndroidElement jardonShoes = driver.findElementByAndroidUIAutomator("text(\"Air Jordan 9 Retro\")");
    //loginPage.jordanShoes.click();
    jardonShoes9.click();
    loginPage.addToCardButton.click();
    loginPage.cardImage.click();

    Assert.assertTrue(loginPage.productImage.isDisplayed());

  }




}
