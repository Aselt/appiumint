package com.techtorial.appium.ecommerce;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

  public LoginPage(AndroidDriver driver){
    PageFactory.initElements(new AppiumFieldDecorator(driver), this);
  }

  @AndroidFindBy(xpath="//android.widget.Spinner[@resource-id='com.androidsample.generalstore:id/spinnerCountry']")
  public AndroidElement country;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Bolivia']")
  AndroidElement Bolivia;

  @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector()).scrollIntoView(text(\"Bolivia\"))")
  AndroidElement bolivia;

  @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter name here']")
  AndroidElement username;

  @AndroidFindBy(id="com.androidsample.generalstore:id/radioFemale")
  AndroidElement genderFemale;

//  @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.RelativeLayout/android.widget.FrameLayout")
//  AndroidElement toastMessage;

  @AndroidFindBy(id = "com.androidsample.generalstore:id/btnLetsShop")
  AndroidElement loginButton;

  @AndroidFindBy(uiAutomator="new UiScrollable(new UiSelector()).scrollIntoView(text(\"Air Jordan 9 Retro\"))")
  public AndroidElement jordanShoes;


  @AndroidFindBy(xpath = "//android.widget.TextView[@text='ADD TO CART']")
  AndroidElement addToCardButton;

  @AndroidFindBy(xpath = "com.androidsample.generalstore:id/appbar_btn_cart")
  AndroidElement cardImage;

  @AndroidFindBy(id = "com.androidsample.generalstore:id/productImage")
  AndroidElement productImage;
}
