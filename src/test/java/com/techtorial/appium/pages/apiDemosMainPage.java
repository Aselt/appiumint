package com.techtorial.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSBy;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class apiDemosMainPage {

  public apiDemosMainPage(AndroidDriver driver){
    PageFactory.initElements(new AppiumFieldDecorator(driver), apiDemosMainPage.class); //--> instead 'this'
  }
  /*Create 5 pages for PageObjectModel;
   -Accessibily
   -main page
   -View page
      -Focus (under views page)
      -Controls(under views)
   -Content


*/
  @AndroidFindBy(id="Accessibility")
  AndroidElement accessibility;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Animation']")
  AndroidElement animation;

  @AndroidFindBy(uiAutomator = "text=\"Content\"")
  AndroidElement content;

  @AndroidFindBy(accessibility = "Animation")
  AndroidElement animationView;

  @AndroidFindBy(id="Controls")
  AndroidElement viewControls;


  @AndroidFindBy(xpath="//android.widget.TextView[@text='1. Light Theme'")
  AndroidElement firstLightTheme;

  @AndroidFindBy(uiAutomator = "text=\"2. Dark Theme\"")
  AndroidElement secondDarkTheme;

  @AndroidFindBy(xpath = "//android.widget.TextView[@text='Focus']")
  AndroidElement viewFocus;





}
