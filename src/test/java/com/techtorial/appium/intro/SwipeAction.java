package com.techtorial.appium.intro;

import com.techtorial.appium.utils.AndroidDriverUtil;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.util.List;

public class SwipeAction {

  AndroidDriver<AndroidElement> driver;

  @Before
  public void setup() throws MalformedURLException {
    DesiredCapabilities caps = AndroidDriverUtil.setupDesiredCapabilities("ApiDemos-debug");
    driver = AndroidDriverUtil.getAndroidDriver(caps);
  }

  @Test
  public void swipe(){
    TouchAction touchAction = new TouchAction(driver);

    AndroidElement views = driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Views\"]"));
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(views))).perform();

    AndroidElement dateWidget = driver.findElementByAndroidUIAutomator("text(\"Date Widgets\")");

    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(dateWidget))).perform();

    AndroidElement inlineText = driver.findElementByAccessibilityId("2. Inline");
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(inlineText))).perform();
  //  inlineText.click();

    AndroidElement clickClock = driver.findElementByAccessibilityId("12");
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(clickClock))).perform();
    AndroidElement num15 = driver.findElement(By.xpath("//android.widget.RadialTimePickerView.RadialPickerTouchHelper[@content-desc='15']"));
    AndroidElement num40 = driver.findElement(By.xpath("//android.widget.RadialTimePickerView.RadialPickerTouchHelper[@content-desc='40']"));

    touchAction.longPress(ElementOption.element(num15)).moveTo(ElementOption.element(num40)).release().perform();
    //touchAction.press(ElementOption.element(num15)).moveTo(ElementOption.element(num40)).perform();

  //not working
    List<AndroidElement> time = driver.findElements(By.xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.TimePicker/android.widget.LinearLayout/android.widget.RelativeLayout"));
    System.out.println(time);
    String finalTime = "";
    for (AndroidElement element : time){
      finalTime += time;
    }
    System.out.println(finalTime);




  }

}
