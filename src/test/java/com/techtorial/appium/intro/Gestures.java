package com.techtorial.appium.intro;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.aspectj.weaver.ast.And;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Gestures {
  DesiredCapabilities desiredCapabilities;
  URL appiumServerUrl;
  AndroidDriver<AndroidElement> driver;

  @Before
  public void setup() throws MalformedURLException {
    File apkFile = new File("src/test/resources/ApiDemos-debug.apk");
    desiredCapabilities = new DesiredCapabilities();
    desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "TestPie");
    desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
    desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
    desiredCapabilities.setCapability(MobileCapabilityType.APP, apkFile.getAbsolutePath());

    appiumServerUrl = new URL("http://localhost:4723/wd/hub");
    driver = new AndroidDriver(appiumServerUrl, desiredCapabilities);
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  }

  @Test
  public void tapGesture() throws MalformedURLException {
    //"attribute("value")"
    TouchAction touchAction = new TouchAction(driver);
    AndroidElement views = driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Views\"]"));
    views.click();

//    AndroidElement customView = driver.findElement(By.xpath("//android.widget.TextView[@content-desc='Custom View']"));
//    customView.click();

    AndroidElement extendbleList = driver.findElementByAccessibilityId("Expandable Lists");
    extendbleList.click();

    AndroidElement customAdapter = driver.findElementByAndroidUIAutomator("text(\"1. Custom Adapter\")");
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(customAdapter))).perform();

    AndroidElement catNames = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.ExpandableListView/android.widget.TextView[3]"));
    Assert.assertEquals("Cat Names", catNames.getText());
  }

  @Test
  public void longPress(){
    TouchAction touchAction = new TouchAction(driver);
    AndroidElement views = driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Views\"]"));
    views.click();


    AndroidElement extendbleList = driver.findElementByAccessibilityId("Expandable Lists");
    extendbleList.click();

    AndroidElement customAdapter = driver.findElementByAndroidUIAutomator("text(\"1. Custom Adapter\")");
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(customAdapter))).perform();

    AndroidElement peopleNames = driver.findElementByAndroidUIAutomator("text(\"People Names\")");
    touchAction.longPress(LongPressOptions.longPressOptions()
            .withDuration(Duration.ofSeconds(2)).withElement(ElementOption.element(peopleNames))).perform();

    AndroidElement sampleMenu = driver.findElementByAndroidUIAutomator("text(\"Sample menu\")");

    Assert.assertEquals("Failded to verify Sample Menu text", "Sample menu", sampleMenu.getText());
    Assert.assertTrue("Failded to verify Sample Menu is displayed", sampleMenu.isDisplayed());
  }

  @Test
  public void findFishNames(){
    TouchAction touchAction = new TouchAction(driver);
    AndroidElement views = driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Views\"]"));
    views.click();


    AndroidElement extendbleList = driver.findElementByAccessibilityId("Expandable Lists");
    extendbleList.click();

    AndroidElement customAdapter = driver.findElementByAndroidUIAutomator("text(\"1. Custom Adapter\")");
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(customAdapter))).perform();



    AndroidElement fishNames = driver.findElementByAndroidUIAutomator("text(\"Fish Names\")");
    touchAction.tap(TapOptions.tapOptions().withElement(ElementOption.element(fishNames))).perform();

    List<AndroidElement> fishNameLists = new ArrayList<>();
    AndroidElement fishNameList1 = driver.findElementByAndroidUIAutomator("text(\"Goldy\")");
    AndroidElement fishNameList2 = driver.findElementByAndroidUIAutomator("text(\"Bubbles\")");
    fishNameLists.add(fishNameList1);
    fishNameLists.add(fishNameList2);
    Assert.assertEquals(2, fishNameLists.size());

    for(AndroidElement element: fishNameLists){
      Assert.assertTrue(element.isDisplayed());
    }


  }
}
