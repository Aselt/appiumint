package com.techtorial.appium.intro;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class LocatorTypes {

  DesiredCapabilities desiredCapabilities;
  URL appiumServerUrl;
  AndroidDriver<AndroidElement> driver;

  @Before
  public void setup() throws MalformedURLException {
    File apkFile = new File("src/test/resources/ApiDemos-debug.apk");
    desiredCapabilities = new DesiredCapabilities();
    desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "TestPie");
    desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
    desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
    desiredCapabilities.setCapability(MobileCapabilityType.APP, apkFile.getAbsolutePath());

    appiumServerUrl = new URL("http://localhost:4723/wd/hub");
    driver = new AndroidDriver(appiumServerUrl, desiredCapabilities);
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  }

  @Test
  public void locators(){
    //accessiblity id
   AndroidElement accessibility = driver.findElementByAccessibilityId("Accessibility");
    accessibility.click();

    //className[@attribute
    AndroidElement customView = driver.findElement(By.xpath("//android.widget.TextView[@content-desc='Custom View']"));
    customView.click();
  }

  @Test
  public void locators2(){
    //resource-id == id in selenium
    AndroidElement views = driver.findElement(By.xpath("//android.widget.TextView[@content-desc=\"Views\"]"));
    views.click();
    //--IQ-- "attribute("value")"
    AndroidElement chronometer = driver.findElementByAndroidUIAutomator("text(\"Chronometer\")");
    chronometer.click();
  }
}
